/* 
    What is Node.JS?

    NodeJS is a runtime environment which allows us to create/develop backend/server-side applications with Javascript

    Because by default, JS was conceptualized solely to the frontend. 

    Why is NodeJS Popular?

    Performance - Node JS is one of the most performing environment for creating backend applications with JS. 

    Familiarity - Since NodeJS is built-aroound and uses JS as its language, it is very familiar for most developers

    NPM - Node Package Manager is the largest registry for node packages. Packagaes are bits of programs, methods. functions, codes that greatly help in the development of an application

    
*/

// console.log("Hello,Node!");

const http = require("http");

// require() - is a built-in JS method which allows us to import packages. Packages are pieces of code we can integrate into our application

// "http" is a default package that comes with NodeJs. it allows us to use methods that let us create server

// http is now a module in your application that works much like an object.

// console.log(http);

// The http module lets us create a server which is able to communicate with a client through the of the Hypertext Transfer Protocol

//When you added the url to the client (browser), the client made a request to the server, localhost:4000
// your server then gave a response, your message

// https://localhost:4000/
// localhost - means your local server/machine
//  4000 - is the port/the process number in your computer

// when you added the url http://localhost:4000 the client triggered the application running on localhost:4000, our NodeJS Server

// createServer() - is a method from the http module. It allows us to creat a server that is able to handle the request of a client and send a response to the client

// createServer() has a function as an argument. This function handles the request and the response. The request object contains details of th request from the client. The response object conatins details of th respones from the server. the createdServer () method always receives the request object first before response

http.createServer(function (request, response) {
    // response.writeHead() - is a method of the response object.. it allows us to add headers to our response. Headers are additional information about our response. We have 2 arguments in the writeHead() method, first is the http status code. An HTTP status is just a numerical code to tell the client about the status of their request. 200 means OK, HTTP 404 means not found.
    // "Content-Type" is one of the most recognizable headers. It simply pertains to the data type of the response

    response.writeHead(200, { "Content-Type": "text/plain" });

    // response.end() it ends our response. Meaning there should no more process/tasks after response.end()
    // it is allso able to send a message/data as string
    response.end(2000);
}).listen(4000);

/* 
    listen() allows us to assign a port to our server. this will allow us to serve/run our index.js serve in our local machine assigned to port 4000
    
*/

console.log("Server is running on localhost:3000");
